import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';

class App extends React.Component{
  render(){
    return (
      <NavigationContainer>
        <View style={styles.tela}>
          <Text style={styles.textoPrincipal}>Hello World!</Text>
        </View>
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  textoPrincipal: {
    fontSize: 35,
    fontWeight: 'bold',
    borderBottomColor: '#000',
    borderStyle: 'solid',
    borderBottomWidth: 1,
  },
  tela: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default App;
